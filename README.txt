Thanks to

https://github.com/rogercodina/node_menusettings_visibility_checkbox

Imagine that you have a content type which nodes should be placed in menu but disabled.
This module just adds an ability to configure a defaulty behavior of menu item visibility on node creation or editing on content type form.